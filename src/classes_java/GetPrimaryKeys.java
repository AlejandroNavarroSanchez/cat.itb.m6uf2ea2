package classes_java;

import java.sql.Connection;
import java.sql.DatabaseMetaData;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;

public class GetPrimaryKeys {

	public static void main(String[] args) {
		 try
		  {
			Class.forName("com.mysql.jdbc.Driver"); //Cargar el driver
			//Establecemos la conexion con la BD
	        Connection conexion = DriverManager.getConnection  
	            ("jdbc:mysql://localhost/school","user1", "pass1");   
	        
	        
	       	    
	  
			DatabaseMetaData dbmd = conexion.getMetaData();//Creamos 
	                               //objeto DatabaseMetaData
	  		ResultSet resul = null;
			 
	  		System.out.println("CLAVE PRIMARIA TABLA ALUMNOS:");
	  		System.out.println("===================================");
	  		ResultSet pk = dbmd.getPrimaryKeys(null, "school", "ALUMNOS");
	  		String pkDep="", separador="";
	  		while (pk.next()) {
	  			   pkDep = pkDep + separador + 
	  		               pk.getString("COLUMN_NAME");//getString(4)
	  			   separador="+";
	  		 }
	  		System.out.println("Clave Primaria: " + pkDep);


				
	  		 conexion.close(); //Cerrar conexion   		  	   
		  } 
	   catch (ClassNotFoundException cn) {cn.printStackTrace();} 
				   catch (SQLException e) {e.printStackTrace();}		
		}//fin de main
	

}
