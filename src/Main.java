import java.io.*;
import java.sql.*;

public class Main {
    public static void main(String[] args) {
        Database db = Database.getInstance();
        db.connect();

//        ex1();
//        ex2();
//        ex3();
        ex4();
//        ex5();
//        ex6();

        db.close();
    }

    /**
     * 1.- Executar el script "school2.sql" que trobaràs al Classroom seguint l'exemple de la classe "script.java" per
     * crear les taules i insertar les dades en la base de dades school2.
     */
    public static void ex1() {
        File scriptFile = new File("school2.sql");
        System.out.println("Fichero de consulta : " + scriptFile.getName());
        System.out.println("Convirtiendo el fichero a cadena...\n");
        BufferedReader entrada = null;
        try {
            entrada = new BufferedReader(new FileReader(scriptFile));
        } catch (FileNotFoundException e) {
            System.err.println("ERROR NO HAY FILE: " + e.getMessage());
        }
        String linea;
        StringBuilder stringBuilder = new StringBuilder();
        String salto = System.getProperty("line.separator");
        try {
            while (true) {
                assert entrada != null;
                if ((linea = entrada.readLine()) == null) break;
                stringBuilder.append(linea);
                stringBuilder.append(salto);
            }
        } catch (IOException e) {
            System.err.println("ERROR de E/S, al operar " + e.getMessage());
        }
        String consulta = stringBuilder.toString();
        System.out.println(consulta);

        try {
            Connection connmysql = Database.getInstance().getConnection();
            Statement sents = connmysql.createStatement();
            int res = sents.executeUpdate(consulta);
            System.out.println("Script creado con éxito, res = " + res);
            sents.close();
        } catch (SQLException e) {
            System.err.println("ERROR AL EJECUTAR EL SCRIPT: " + e.getMessage());
        }
    }

    /**
     * 2.- Mostra la informació de la base de dades seguint l'exemple de la classe "DatabaseMetadata.java".
     */
    public static void ex2() {
        try
        {
            //Establecemos la conexion con la BD
            Connection conexion = Database.getInstance().getConnection();

            DatabaseMetaData dbmd = conexion.getMetaData();//Creamos
            //objeto DatabaseMetaData
            ResultSet resul = null;

            String nombre  = dbmd.getDatabaseProductName();
            String driver  = dbmd.getDriverName();
            String url     = dbmd.getURL();
            String usuario = dbmd.getUserName() ;

            System.out.println("INFORMACIÓN SOBRE LA BASE DE DATOS:");
            System.out.println("===================================");
            System.out.printf("Nombre : %s %n", nombre );
            System.out.printf("Driver : %s %n", driver );
            System.out.printf("URL    : %s %n", url );
            System.out.printf("Usuario: %s %n", usuario );

            //Obtener informaci�n de las tablas y vistas que hay
            resul = dbmd.getTables(null, "ejemplo", null, null);

            while (resul.next()) {
                String catalogo = resul.getString(1);//columna 1
                String esquema = resul.getString(2); //columna 2
                String tabla = resul.getString(3);   //columna 3
                String tipo = resul.getString(4);	//columna 4
                System.out.printf("%s - Catalogo: %s, Esquema: %s, Nombre: %s %n",
                        tipo, catalogo, esquema, tabla);
            }
        } catch (SQLException e) {e.printStackTrace();}
    }

    /**
     * 3.- Mostra la informació dels camps de totes les taules creades seguint l'exemple de la classe
     * "ResultsetMetadata.java".
     */
    public static void ex3() {
        try {
            Connection conexion = Database.getInstance().getConnection();

            Statement sentencia = conexion.createStatement();
            ResultSet rs = sentencia.executeQuery("SELECT * FROM alumnos, asignaturas, notas");

            ResultSetMetaData rsmd = rs.getMetaData();

            int nColumnas = rsmd.getColumnCount();
            String nula;
            System.out.printf("Número de columnas recuperadas: %d%n", nColumnas);
            for (int i = 1; i <= nColumnas; i++) {
                System.out.printf("Columna %d: %n ", i);
                System.out.println("\tTabla: " + rsmd.getTableName(i));
                System.out.printf("\tNombre: %s %n\tTipo: %s %n",
                        rsmd.getColumnName(i),  rsmd.getColumnTypeName(i));
                if (rsmd.isNullable(i) == ResultSetMetaData.columnNoNulls)
                    nula = "NO";
                else
                    nula = "SI";

                System.out.printf("\tPuede ser nula?: %s %n ", nula);
                System.out.printf("\tMáximo ancho de la columna: %d %n\n",
                        rsmd.getColumnDisplaySize(i));
            }
            sentencia.close();
            rs.close();

        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    /**
     * 4.- Mostra la informació dels camps de totes les taules creades seguint l'exemple de la classe
     * "GetColumns.java".
     */
    public static void ex4() {
        try {
            Connection conexion = Database.getInstance().getConnection();

            DatabaseMetaData dbmd = conexion.getMetaData();//Creamos
            //objeto DatabaseMetaData
            ResultSet resul = null;

            String[] tables = {"alumnos", "asignaturas", "notas"};

            for (String t : tables) {

            }
            System.out.println("COLUMNAS TABLA ALUMNOS:");
            System.out.println("===================================");
            ResultSet columnas=null;
            columnas = dbmd.getColumns(null, "school", "ALUMNOS", null);
            while (columnas.next()) {
                String nombCol = columnas.getString("COLUMN_NAME"); //getString(4)
                String tipoCol = columnas.getString("TYPE_NAME");   //getString(6)
                String tamCol = columnas.getString("COLUMN_SIZE");  //getString(7)
                String nula  = columnas.getString("IS_NULLABLE");   //getString(18)

                System.out.printf("Columna: %s, Tipo: %s, Tamaño: %s, ¿Puede ser Nula?: %s %n", nombCol, tipoCol, tamCol, nula);
            }
        }
        catch (SQLException e) {e.printStackTrace();}
    }

    /**
     * 5.- Mostra les claus primaries de totes les taules seguint l'exemple de la classe "GetPrimaryKeys.java".
     */
    public static void ex5() {

    }

    /**
     * 6.- Mostra les claus foràneas de totes les taules seguint l'exemple de la classe "GetExportedKeys.java".
     */
    public static void ex6() {

    }
}
