import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class Database {
    //informacio de la bdd
    private static final String URL = "jdbc:postgresql://surus.db.elephantsql.com:5432/"; // URL
    private static final String BD = "ulzduoih"; // BD
    private static final String USER = "ulzduoih"; // USER
    private static final String PASS = "S8F66iXf6n9ZbfNgrqiaX_bwOiV4_lug"; // PASS

    //instancia de la bdd
    private static Database database = null;
    public static Database getInstance(){
        if(database == null)
            database = new Database();
        return database;
    }

    private Connection connection;

    //constructor
    public Database(){
        connection = null;
    }

    //metode per establir la conexio amb la bdd
    public void connect(){
        try {
            connection = DriverManager.getConnection(URL + BD, USER, PASS);
            System.out.println("[" +
                    "\u001b[96mDB" +
                    "\u001b[0m::" +
                    "\u001b[92mCONNECTED" +
                    "\u001b[0m]\n");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    //metode per retornar una conexio establerta
    public java.sql.Connection getConnection() {
        return connection;
    }

    //metode per terminar la conexio amb la bdd
    public void close(){
        try {
            connection.close();
            System.out.println("\n[" +
                    "\u001b[96mDB" +
                    "\u001b[0m::" +
                    "\u001b[91mCLOSED" +
                    "\u001b[0m]");
        }catch(SQLException e){
            System.err.println("Error tancant la BD");
        }
        connection = null;
    }
}
